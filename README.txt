CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainer


INTRODUCTION
------------

This module(Amazing-forms) provides a custom form in modal using Drupal's
FormBuilder and AJAX API.
Amazing-forms module provide a url(../amazing-forms/demo),once you hit the url,
welcome page of the form will open.
There is a button link(click here to open form), once you click on button, Form
will open on modal popup.



REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommended module is needed.

INSTALLATION
------------

* Install a contributed Drupal module.
* See also @link Installing Drupal 8 Modules.
* @link
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
web page explaining it further. @endlink

CONFIGURATION
-------------

No configuration is needed.

MAINTAINERS
-----------

Current maintainers:
 * sunil-singh -https://www.drupal.org/u/sunil-singh
